package at.waldhart.wsdemo.utility;

import at.waldhart.wsdemo.models.TdArticle;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class TestWsUtility {

    @Test
    void testMergeObjects() {
        TdArticle art1 = new TdArticle();
        art1.setArtNo(1L);
        art1.setArtName("Art1");
        art1.setArtType((short) 0);
        art1.setArtActive((short) 0);

        TdArticle art2 = new TdArticle();
        art2.setArtNo(2L);
        art2.setArtName("Art2");
        art2.setArtType((short) 1);
        art2.setArtActive((short) 1);

        TdArticle merge1 = WsUtility.mergeObjects(art1, art2);
        assertEquals(art1, merge1);

        merge1 = WsUtility.mergeObjects(art2, art1);
        assertEquals(art2, merge1);

        TdArticle art3 = new TdArticle();
        art3.setArtNo(1L);
        art3.setArtName("Art1");

        TdArticle art4 = new TdArticle();
        art4.setArtType((short) 1);
        art4.setArtActive((short) 1);

        TdArticle target1 = new TdArticle();
        target1.setArtNo(1L);
        target1.setArtName("Art1");
        target1.setArtType((short) 1);
        target1.setArtActive((short) 1);

        merge1 = WsUtility.mergeObjects(art3, art4);
        assertEquals(target1, merge1);

        merge1 = WsUtility.mergeObjects(art4, art3);
        assertEquals(target1, merge1);
    }
}

