package at.waldhart.wsdemo.dtos.mappers;

import at.waldhart.wsdemo.dtos.TfEventDTO;
import at.waldhart.wsdemo.models.TfEvent;
import org.springframework.stereotype.Component;

@Component
public class TfEventDtoMapper {
    public TfEventDTO toDto(final TfEvent originalModel) {
        TfEventDTO newDto = new TfEventDTO();

        newDto.setEvnNo(originalModel.getEvnNo());
        newDto.setEvnDateStartday(originalModel.getEvnDateStartday());
        newDto.setEvnArticle(originalModel.getEvnArticle().getArtNo());

        return newDto;
    }

}
