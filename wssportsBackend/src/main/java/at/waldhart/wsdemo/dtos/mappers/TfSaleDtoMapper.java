package at.waldhart.wsdemo.dtos.mappers;

import at.waldhart.wsdemo.dtos.TfEventDTO;
import at.waldhart.wsdemo.dtos.TfSaleDTO;
import at.waldhart.wsdemo.models.TfSale;
import org.springframework.stereotype.Component;

@Component
public class TfSaleDtoMapper {
    public TfSaleDTO toDto(final TfSale originalModel) {
        TfSaleDTO newDto = new TfSaleDTO();

        newDto.setSalNo(originalModel.getSalNo());
        newDto.setSalPrice(originalModel.getSalPrice().getPrcPrice());
        newDto.setSalType(originalModel.getSalType());
        newDto.setSalDateFrom(originalModel.getSalDateFrom());
        newDto.setSalDateTo(originalModel.getSalDateTo());
        newDto.setSalAdditionalProperties(originalModel.getSalAdditionalProperties());
        newDto.setSalArticle(originalModel.getSalArticle().getArtNo());

        return newDto;
    }

}
