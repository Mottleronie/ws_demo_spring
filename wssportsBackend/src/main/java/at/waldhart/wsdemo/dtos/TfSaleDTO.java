package at.waldhart.wsdemo.dtos;

import at.waldhart.wsdemo.models.TdArticle;
import at.waldhart.wsdemo.models.TdPrice;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

public class TfSaleDTO {

    private Long salNo;
    private Short salType;
    private Date salDateFrom;
    private Date salDateTo;
    private Object salAdditionalProperties;
    private Long salArticle;
    private Short salPrice;

    public TfSaleDTO(){}

    public TfSaleDTO(final Long salNo,
                     final Short salType,
                     final Date salDateFrom,
                     final Date salDateTo,
                     final Object salAdditionalProperties,
                     final Long salArticle,
                     final Short salPrice){
        this.salNo = salNo;
        this.salType = salType;
        this.salDateFrom = salDateFrom;
        this.salDateTo = salDateTo;
        this.salAdditionalProperties = salAdditionalProperties;
        this.salArticle = salArticle;
        this.salPrice = salPrice;
    }

    /* getter and setter */
    public Long getSalNo() {
        return salNo;
    }

    public void setSalNo(final Long salNo) {
        this.salNo = salNo;
    }

    public Short getSalType() {
        return salType;
    }

    public void setSalType(final Short salType) {
        this.salType = salType;
    }

    public Date getSalDateFrom() {
        return salDateFrom;
    }

    public void setSalDateFrom(final Date salDateFrom) {
        this.salDateFrom = salDateFrom;
    }

    public Date getSalDateTo() {
        return salDateTo;
    }

    public void setSalDateTo(final Date salDateTo) {
        this.salDateTo = salDateTo;
    }

    public Object getSalAdditionalProperties() {
        return salAdditionalProperties;
    }

    public void setSalAdditionalProperties(Object salAdditionalProperties) {
        this.salAdditionalProperties = salAdditionalProperties;
    }

    public Long getSalArticle() {
        return salArticle;
    }

    public void setSalArticle(final Long salArticle) {
        this.salArticle = salArticle;
    }

    public Short getSalPrice() {
        return salPrice;
    }

    public void setSalPrice(final Short salPrice) {
        this.salPrice = salPrice;
    }


}

