package at.waldhart.wsdemo.dtos;

import at.waldhart.wsdemo.models.TdArticle;
import at.waldhart.wsdemo.models.TdPrice;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.Type;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

public class TdPriceDTO {

    private Long prcNo;

    private Long prcArticle;

    private Date prcDateFrom;

    private Date prcDateTo;

    private Short prcPrice;

    private Short prcPercentage;


    public Long getPrcArticle() {
        return prcArticle;
    }

    public void setPrcArticle(final Long prcArticle) {
        this.prcArticle = prcArticle;
    }

    public Date getPrcDateFrom() {
        return prcDateFrom;
    }

    public void setPrcDateFrom(final Date prcDateFrom) {
        this.prcDateFrom = prcDateFrom;
    }

    public Date getPrcDateTo() {
        return prcDateTo;
    }

    public void setPrcDateTo(final Date prcDateTo) {
        this.prcDateTo = prcDateTo;
    }

    public Short getPrcPrice() {
        return prcPrice;
    }

    public void setPrcPrice(final Short prcPrice) {
        this.prcPrice = prcPrice;
    }

    public Short getPrcPercentage() {
        return prcPercentage;
    }

    public void setPrcPercentage(final Short prcPercentage) {
        this.prcPercentage = prcPercentage;
    }

    public Long getPrcNo() {
        return prcNo;
    }

    public void setPrcNo(final Long prcNo) {
        this.prcNo = prcNo;
    }

}

