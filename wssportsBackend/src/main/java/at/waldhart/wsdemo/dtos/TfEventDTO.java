package at.waldhart.wsdemo.dtos;

import at.waldhart.wsdemo.models.TdArticle;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

public class TfEventDTO {

    private Long evnNo;
    private Long evnArticle;
    private Date evnDateStartday;

    /* getter and setter */
    public Long getEvnNo() {
        return evnNo;
    }

    public void setEvnNo(final Long evnNo) {
        this.evnNo = evnNo;
    }

    public Date getEvnDateStartday() {
        return evnDateStartday;
    }

    public void setEvnDateStartday(final Date evnDateStartday) {
        this.evnDateStartday = evnDateStartday;
    }

    public Long getEvnArticle() {
        return evnArticle;
    }

    public void setEvnArticle(final Long evnArticle) {
        this.evnArticle = evnArticle;
    }


}

