package at.waldhart.wsdemo.repositories;

import at.waldhart.wsdemo.models.TdArticle;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TdArticleRepository extends CrudRepository<TdArticle, Long> {

    List<TdArticle> findByArtName(String artName);

    List<TdArticle> findAll();

    TdArticle findByArtNo(long artNo);
}