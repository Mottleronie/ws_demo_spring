package at.waldhart.wsdemo.repositories;

import at.waldhart.wsdemo.models.TdPrice;
import at.waldhart.wsdemo.models.TfEvent;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TfEventRepository extends CrudRepository<TfEvent, Long> {

    List<TfEvent> findAll();

    TfEvent findByEvnNo(long evnNo);

    List<TfEvent> findByEvnArticleArtNo(Long artNo);
}
