package at.waldhart.wsdemo.repositories;

import at.waldhart.wsdemo.models.TdPrice;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TdPriceRepository extends CrudRepository<TdPrice, Long> {

    List<TdPrice> findAll();

    List<TdPrice> findByPrcArticleArtNo(Long artNo);

    TdPrice findByPrcNo(long prcNo);
}
