package at.waldhart.wsdemo.errors.exceptions;

public class DeletionFailedException extends RuntimeException {
    private static final String defaultMessage = "Cannot delete this entity... Have you already deleted this entity?";



    public DeletionFailedException() {
        super(DeletionFailedException.defaultMessage);
    }

    public DeletionFailedException(final Long requestedId) {
        super(DeletionFailedException.defaultMessage+" (ID="+requestedId+")");
    }

    public DeletionFailedException(final Long requestedId, final Class entityClass) {
        super("The requested "+entityClass.getSimpleName()+" (Package="+entityClass.getPackageName()+") with ID="+requestedId+" could not be deleted... Have you already deleted this one?");
    }
}
