package at.waldhart.wsdemo.errors.exceptions;

public class EntityNotFoundException extends RuntimeException {
    private static final String defaultMessage = "The requested entity was not found...";



    public EntityNotFoundException() {
        super(EntityNotFoundException.defaultMessage);
    }

    public EntityNotFoundException(final Long requestedId) {
        super(EntityNotFoundException.defaultMessage+" (ID="+requestedId+")");
    }

    public EntityNotFoundException(final Long requestedId, final Class entityClass) {
        super("The requested "+entityClass.getSimpleName()+" (Package="+entityClass.getPackageName()+") with ID="+requestedId+" could not be found...");
    }
}
