package at.waldhart.wsdemo.services;

import at.waldhart.wsdemo.dtos.TfSaleDTO;
import at.waldhart.wsdemo.errors.exceptions.*;
import at.waldhart.wsdemo.models.TfEvent;
import at.waldhart.wsdemo.models.TfSale;
import at.waldhart.wsdemo.repositories.TfSaleRepository;

import at.waldhart.wsdemo.utility.WsUtility;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class TfSaleService {

    @Autowired
    TfSaleRepository TfSaleRepo;

    @Autowired
    ObjectMapper objectMapper;

    public List<TfSale> getTfSale() {
        return this.TfSaleRepo.findAll();
    }

    public List<TfSaleDTO> getTfSaleDto() {
        return this.TfSaleRepo.findAllTfSaleDTO();
    }

    public TfSale getTfSale(Long id) {
        TfSale foundEntity = this.TfSaleRepo.findBySalNo(id);
        if(foundEntity == null){
            throw new EntityNotFoundException(id, TfSale.class);
        }
        return foundEntity;
    }

    public TfSale createTfSale(final TfSale newEntity) {
        return this.TfSaleRepo.save(newEntity);
    }

    public TfSale updateTfSale(final Long id, final TfSale updateEntity) {
        if(this.TfSaleRepo.findById(id) == null){
            throw new CannotUpdateNonExistingEntity(id, TfSale.class);
        }

        updateEntity.setSalNo(id);

        try{
            return this.TfSaleRepo.save(updateEntity);
        }
        catch(Exception e){
            throw new UpdateEntityFailedException(id, updateEntity);
        }
    }

    public TfSale patchTfSale(final Long id, final String patchRequestString){
        try {
            TfSale originalEntity = this.getTfSale(id);
            TfSale patchEntity = objectMapper.readValue(patchRequestString, TfSale.class);

            List<String> deletedFieldsList = new ArrayList<>();
            Map<String, Object> objToPatch = objectMapper.readValue(patchRequestString, Map.class);
            for (Map.Entry<String, Object> entry : objToPatch.entrySet()) {
                if(entry.getValue() == null){
                    deletedFieldsList.add(entry.getKey());
                }
            }

            originalEntity = WsUtility.patchObjects(patchEntity, originalEntity, deletedFieldsList);

            return this.updateTfSale(id, originalEntity);
        }
        catch (Exception e) {
            System.out.println(e);
            throw new CannotUpdateNonExistingEntity(id);
        }
    }

    public void deleteTfSale(final Long id) {
        try{
            this.TfSaleRepo.deleteById(id);
        }
        catch(EmptyResultDataAccessException e){
            throw new CannotDeleteNonExistingEntity(id, TfSale.class);
        }
        catch(Exception e){
            throw new DeletionFailedException(id, TfSale.class);
        }
    }

}
