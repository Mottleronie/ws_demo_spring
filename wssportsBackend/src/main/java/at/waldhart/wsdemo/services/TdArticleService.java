package at.waldhart.wsdemo.services;

import at.waldhart.wsdemo.errors.exceptions.*;
import at.waldhart.wsdemo.models.TdArticle;
import at.waldhart.wsdemo.models.TfEvent;
import at.waldhart.wsdemo.repositories.TdArticleRepository;
import at.waldhart.wsdemo.utility.WsUtility;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class TdArticleService {

    @Autowired
    TdArticleRepository articleRepo;

    @Autowired
    ObjectMapper objectMapper;

    public List<TdArticle> getArticles() {
        return this.articleRepo.findAll();
    }

    public TdArticle getArticle(final Long artNo) {
        TdArticle foundArticle = this.articleRepo.findByArtNo(artNo);
        if(foundArticle == null){
            throw new EntityNotFoundException(artNo, TdArticle.class);
        }
        return foundArticle;
    }

    public TdArticle createArticle(final TdArticle newArticle) {
        return this.articleRepo.save(newArticle);
    }

    public TdArticle updateArticle(final Long artNo, final TdArticle updateEntity) {
        if(this.articleRepo.findByArtNo(artNo) == null){
            throw new CannotUpdateNonExistingEntity(artNo, TdArticle.class);
        }

        updateEntity.setArtNo(artNo);

        try{
            return this.articleRepo.save(updateEntity);
        }
        catch(Exception e){
            throw new UpdateEntityFailedException(artNo, updateEntity);
        }
    }

    public TdArticle patchArticle(final Long artNo, final String patchRequestString){
        try {
            TdArticle originalEntity = this.getArticle(artNo);
            TdArticle patchEntity = objectMapper.readValue(patchRequestString, TdArticle.class);

            List<String> deletedFieldsList = new ArrayList<>();
            Map<String, Object> objToPatch = objectMapper.readValue(patchRequestString, Map.class);
            for (Map.Entry<String, Object> entry : objToPatch.entrySet()) {
                if(entry.getValue() == null){
                    deletedFieldsList.add(entry.getKey());
                }
            }

            originalEntity = WsUtility.patchObjects(patchEntity, originalEntity, deletedFieldsList);

            return this.updateArticle(artNo, originalEntity);
        }
        catch (Exception e) {
            System.out.println(e);
            throw new CannotUpdateNonExistingEntity(artNo);
        }
    }

    public void deleteTfArticle(final Long id) {
        try{
            //this.articleRepo.deleteById(id);
        }
        catch(EmptyResultDataAccessException e){
            throw new CannotDeleteNonExistingEntity(id, TdArticle.class);
        }
        catch(Exception e){
            System.out.println(e);
            throw new DeletionFailedException(id, TdArticle.class);
        }
    }

}
