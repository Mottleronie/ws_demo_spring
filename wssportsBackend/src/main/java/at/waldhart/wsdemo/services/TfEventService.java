package at.waldhart.wsdemo.services;

import at.waldhart.wsdemo.errors.exceptions.*;
import at.waldhart.wsdemo.models.TfEvent;
import at.waldhart.wsdemo.repositories.TfEventRepository;

import at.waldhart.wsdemo.utility.WsUtility;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class TfEventService {

    @Autowired
    TfEventRepository TfEventRepo;

    @Autowired
    ObjectMapper objectMapper;

    public List<TfEvent> getTfEvent() {
            return this.TfEventRepo.findAll();
        }

    public TfEvent getTfEvent(Long id) {
        TfEvent foundEntity = this.TfEventRepo.findByEvnNo(id);
        if(foundEntity == null){
            throw new EntityNotFoundException(id, TfEvent.class);
        }
        return foundEntity;
    }

    public List<TfEvent> getTfEventForTdArticle(final Long artNo) {
        return this.TfEventRepo.findByEvnArticleArtNo(artNo);
    }

    public TfEvent createTfEvent(final TfEvent newEntity) {
        return this.TfEventRepo.save(newEntity);
    }

    public TfEvent updateTfEvent(final Long id, final TfEvent updateEntity) {
        if(this.TfEventRepo.findById(id) == null){
            throw new CannotUpdateNonExistingEntity(id, TfEvent.class);
        }

        updateEntity.setEvnNo(id);

        try{
            return this.TfEventRepo.save(updateEntity);
        }
        catch(Exception e){
            throw new UpdateEntityFailedException(id, updateEntity);
        }
    }

    public TfEvent patchTfEvent(final Long id, final String patchRequestString){
        try {
            TfEvent originalEntity = this.getTfEvent(id);
            TfEvent patchEntity = objectMapper.readValue(patchRequestString, TfEvent.class);

            List<String> deletedFieldsList = new ArrayList<>();
            Map<String, Object> objToPatch = objectMapper.readValue(patchRequestString, Map.class);
            for (Map.Entry<String, Object> entry : objToPatch.entrySet()) {
                if(entry.getValue() == null){
                    deletedFieldsList.add(entry.getKey());
                }
            }

            originalEntity = WsUtility.patchObjects(patchEntity, originalEntity, deletedFieldsList);

            return this.updateTfEvent(id, originalEntity);
        }
        catch (Exception e) {
            System.out.println(e);
            throw new CannotUpdateNonExistingEntity(id);
        }
    }

    public void deleteTfEvent(final Long id) {
        try{
            this.TfEventRepo.deleteById(id);
        }
        catch(EmptyResultDataAccessException e){
            throw new CannotDeleteNonExistingEntity(id, TfEvent.class);
        }
        catch(Exception e){
            throw new DeletionFailedException(id, TfEvent.class);
        }
    }

}
