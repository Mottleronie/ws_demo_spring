package at.waldhart.wsdemo.services;

import at.waldhart.wsdemo.errors.exceptions.*;
import at.waldhart.wsdemo.models.TdPrice;
import at.waldhart.wsdemo.models.TfEvent;
import at.waldhart.wsdemo.repositories.TdPriceRepository;

import at.waldhart.wsdemo.utility.WsUtility;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class TdPriceService {

    @Autowired
    TdPriceRepository TdPriceRepo;

    @Autowired
    ObjectMapper objectMapper;

    public List<TdPrice> getTdPrice() {
        return this.TdPriceRepo.findAll();
    }

    public TdPrice getTdPrice(final Long id) {
        TdPrice foundEntity = this.TdPriceRepo.findByPrcNo(id);

        if(foundEntity == null){
            throw new EntityNotFoundException(id, TdPrice.class);
        }
        return foundEntity;
    }

    public List<TdPrice> getTdPricesForTdArticle(final Long artNo) {
        return this.TdPriceRepo.findByPrcArticleArtNo(artNo);
    }

    public TdPrice createTdPrice(TdPrice newEntity) {
        try{
            return this.TdPriceRepo.save(newEntity);
        }
        catch(Exception e){
            throw new UpdateEntityFailedException(-1L, newEntity);
        }

    }

    public TdPrice updateTdPrice(final Long id, final TdPrice updateEntity) {
        if(this.TdPriceRepo.findById(id) == null){
            throw new CannotUpdateNonExistingEntity(id, TdPrice.class);
        }

        updateEntity.setPrcNo(id);

        try{
            return this.TdPriceRepo.save(updateEntity);
        }
        catch(Exception e){
            throw new UpdateEntityFailedException(id, updateEntity);
        }
    }

    public TdPrice patchTdPrice(final Long id, final String patchRequestString){
        try {
            TdPrice originalEntity = this.getTdPrice(id);
            TdPrice patchEntity = objectMapper.readValue(patchRequestString, TdPrice.class);

            List<String> deletedFieldsList = new ArrayList<>();
            Map<String, Object> objToPatch = objectMapper.readValue(patchRequestString, Map.class);
            for (Map.Entry<String, Object> entry : objToPatch.entrySet()) {
                if(entry.getValue() == null){
                    deletedFieldsList.add(entry.getKey());
                }
            }

            originalEntity = WsUtility.patchObjects(patchEntity, originalEntity, deletedFieldsList);

            return this.updateTdPrice(id, originalEntity);
        }
        catch (Exception e) {
            System.out.println(e);
            throw new CannotUpdateNonExistingEntity(id);
        }
    }

    public void deleteTdPrice(final Long id) {
        try{
            this.TdPriceRepo.deleteById(id);
        }
        catch(EmptyResultDataAccessException e){
            throw new CannotDeleteNonExistingEntity(id, TdPrice.class);
        }
        catch(Exception e){
            throw new DeletionFailedException(id, TdPrice.class);
        }
    }
}
