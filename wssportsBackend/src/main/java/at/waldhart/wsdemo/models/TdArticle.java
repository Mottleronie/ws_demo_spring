package at.waldhart.wsdemo.models;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public class TdArticle {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Long artNo;

    @Size(max=100)
    private String artName;

    @NotNull
    @Type(type = "org.hibernate.type.ShortType")
    private Short artType;

    @NotNull
    @Type(type = "org.hibernate.type.ShortType")
    private Short artActive;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private Object artInformation;

    @OneToMany(mappedBy = "prcArticle")
    private Set<TdPrice> artPrices;

    /* getter and setter */
    public Long getArtNo() {
        return artNo;
    }

    public String getArtName() {
        return artName;
    }

    public Short getArtType() {
        return artType;
    }

    public Short getArtActive() {
        return artActive;
    }

    public void setArtNo(final Long artNo) {
        this.artNo = artNo;
    }

    public void setArtName(final String artName) {
        this.artName = artName;
    }

    public void setArtType(final Short artType) {
        this.artType = artType;
    }

    public void setArtActive(final Short artActive) {
        this.artActive = artActive;
    }

    public Object getArtInformation() { return this.artInformation; }

    public void setArtInformation(final Object artInformation) { this.artInformation = artInformation; }


    @Override
    public String toString() {
        return "[Article#"+this.artNo+"]: {artName="+this.artName+", artType="+this.artType+", artActive="+this.artActive+"}";
    }

    @Override
    public boolean equals(final Object obj) {
        if(!this.getClass().isInstance(obj)){
            return false;
        }
        TdArticle other = (TdArticle) obj;
        if(this.getArtNo() != other.getArtNo()){
            return false;
        }
        if(this.getArtName() != other.getArtName()){
            return false;
        }
        if(this.getArtActive() != other.getArtActive()){
            return false;
        }
        if(this.getArtType() != other.getArtType()){
            return false;
        }
        if((this.getArtInformation() != null) &&
                !this.getArtInformation().equals(other.getArtInformation())){
            return false;
        }
        if(this.getArtNo() != other.getArtNo()){
            return false;
        }

        return true;
    }

}
