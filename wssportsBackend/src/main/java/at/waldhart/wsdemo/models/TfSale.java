package at.waldhart.wsdemo.models;

import at.waldhart.wsdemo.dtos.TfSaleDTO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
public class TfSale {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Long salNo;

    @NotNull
    @Type(type = "org.hibernate.type.ShortType")
    private Short salType;

    @NotNull
    @Type(type = "org.hibernate.type.DateType")
    private Date salDateFrom;

    @NotNull
    @Type(type = "org.hibernate.type.DateType")
    private Date salDateTo;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private Object salAdditionalProperties;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name="sal_art_no")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private TdArticle salArticle;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name="sal_prc_no")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private TdPrice salPrice;

    /* getter and setter */
    public Long getSalNo() {
        return salNo;
    }

    public void setSalNo(final Long salNo) {
        this.salNo = salNo;
    }

    public Short getSalType() {
        return salType;
    }

    public void setSalType(final Short salType) {
        this.salType = salType;
    }

    public Date getSalDateFrom() {
        return salDateFrom;
    }

    public void setSalDateFrom(final Date salDateFrom) {
        this.salDateFrom = salDateFrom;
    }

    public Date getSalDateTo() {
        return salDateTo;
    }

    public void setSalDateTo(final Date salDateTo) {
        this.salDateTo = salDateTo;
    }

    public Object getSalAdditionalProperties() {
        return salAdditionalProperties;
    }

    public void setSalAdditionalProperties(Object salAdditionalProperties) {
        this.salAdditionalProperties = salAdditionalProperties;
    }

    public TdArticle getSalArticle() {
        return salArticle;
    }

    public void setSalArticle(final TdArticle salArticle) {
        this.salArticle = salArticle;
    }

    public TdPrice getSalPrice() {
        return salPrice;
    }

    public void setSalPrice(final TdPrice salPrice) {
        this.salPrice = salPrice;
    }

    @Override
    public String toString() {
        return "[Sale#"+this.salNo+"]: {type="+this.salType+
                ", range="+this.salDateFrom+"-"+this.salDateTo+
                ", article="+this.salArticle.getArtName()+
                "(#"+this.salArticle.getArtNo()+"), price="+
                salPrice.getPrcPrice()+"(#"+this.getSalPrice().getPrcNo()+")}";
    }

}
