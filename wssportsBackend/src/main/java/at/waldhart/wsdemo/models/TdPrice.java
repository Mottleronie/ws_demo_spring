package at.waldhart.wsdemo.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.Date;

@Entity
public class TdPrice {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Long prcNo;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name="prc_art_no")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private TdArticle prcArticle;

    @NotNull
    @Type(type = "org.hibernate.type.DateType")
    private Date prcDateFrom;

    @NotNull
    @Type(type = "org.hibernate.type.DateType")
    private Date prcDateTo;

    @NotNull
    private Short prcPrice;

    @NotNull
    private Short prcPercentage;


    public TdArticle getPrcArticle() {
        return prcArticle;
    }

    public void setPrcArticle(final TdArticle prcArticle) {
        this.prcArticle = prcArticle;
    }

    public Date getPrcDateFrom() {
        return prcDateFrom;
    }

    public void setPrcDateFrom(final Date prcDateFrom) {
        this.prcDateFrom = prcDateFrom;
    }

    public Date getPrcDateTo() {
        return prcDateTo;
    }

    public void setPrcDateTo(final Date prcDateTo) {
        this.prcDateTo = prcDateTo;
    }

    public Short getPrcPrice() {
        return prcPrice;
    }

    public void setPrcPrice(final Short prcPrice) {
        this.prcPrice = prcPrice;
    }

    public Short getPrcPercentage() {
        return prcPercentage;
    }

    public void setPrcPercentage(final Short prcPercentage) {
        this.prcPercentage = prcPercentage;
    }

    public Long getPrcNo() {
        return prcNo;
    }

    public void setPrcNo(final Long prcNo) {
        this.prcNo = prcNo;
    }

    // TODO: adapt with new fields
    @Override
    public String toString() {
        return "[Price#"+this.prcNo+"]: {article: "+this.prcArticle.getArtName()+"["+this.prcArticle.getArtNo()+
                "],valid: "+this.prcDateFrom+"-"+this.prcDateTo+", price: "+this.prcPrice+
                ", percentage: "+this.prcPercentage+"%}";
    }

}
