package at.waldhart.wsdemo.controllers;

import at.waldhart.wsdemo.dtos.TdPriceDTO;
import at.waldhart.wsdemo.dtos.mappers.TdPriceDtoMapper;
import at.waldhart.wsdemo.models.TdPrice;
import at.waldhart.wsdemo.services.TdPriceService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static java.util.stream.Collectors.toList;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/prices")
public class TdPriceController {

    @Autowired
    TdPriceService TdPriceService;
    @Autowired
    TdPriceDtoMapper mapper;


    @RequestMapping(value="/", method= RequestMethod.GET)
    @Operation(
            summary = "Returns all price definitions of all articles",
            description = "With this description you should better understand how this automatic API documentation works"
    )
    @ResponseBody
    public List<TdPriceDTO> getAllTdPrice(){
        long start = System.currentTimeMillis();
        List<TdPriceDTO> returnPayload = this.TdPriceService.getTdPrice().stream().map(mapper::toDto).collect(toList());
        long finish = System.currentTimeMillis();
        System.out.println("### Timemeasure [TdPriceController::getAllTdPrice]: "+(finish-start)+"ms");

        return returnPayload;
    }

    @RequestMapping(value="/{id}", method= RequestMethod.GET)
    @ResponseBody
    public TdPriceDTO getTdPrice(@PathVariable final Long id){
        long start = System.currentTimeMillis();
        TdPriceDTO rtn = mapper.toDto(this.TdPriceService.getTdPrice(id));
        long finish = System.currentTimeMillis();
        System.out.println("### Timemeasure [TdPriceController::getTdPrice]: "+(finish-start)+"ms");

        return rtn;
    }

    @RequestMapping(value="/", method= RequestMethod.POST)
    @ResponseBody
    public TdPrice createTdPrice(@RequestBody final TdPrice newEntity){
        long start = System.currentTimeMillis();
        TdPrice rtn = this.TdPriceService.createTdPrice(newEntity);
        long finish = System.currentTimeMillis();
        System.out.println("### Timemeasure [TdPriceController::createTdPrice]: "+(finish-start)+"ms");

        return rtn;
    }

    @RequestMapping(value="/{id}", method= RequestMethod.PUT)
    @ResponseBody
    public TdPrice updateTdPrice(@RequestBody final TdPrice updateEntity, @PathVariable final Long id){
        long start = System.currentTimeMillis();
        TdPrice rtn = this.TdPriceService.updateTdPrice(id, updateEntity);
        long finish = System.currentTimeMillis();
        System.out.println("### Timemeasure [TdPriceController::updateTdPrice]: "+(finish-start)+"ms");

        return rtn;
    }

    @RequestMapping(value="/{id}", method= RequestMethod.PATCH)
    @ResponseBody
    public TdPrice patchTdPrice(@RequestBody final String patch, @PathVariable final Long id){
        long start = System.currentTimeMillis();
        TdPrice rtn = this.TdPriceService.patchTdPrice(id, patch);
        long finish = System.currentTimeMillis();
        System.out.println("### Timemeasure [TdPriceController::patchTdPrice]: "+(finish-start)+"ms");

        return rtn;
    }

    @RequestMapping(value="/{id}", method= RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<Object> deleteTdPrice(@PathVariable final Long id){
        long start = System.currentTimeMillis();
        this.TdPriceService.deleteTdPrice(id);
        long finish = System.currentTimeMillis();
        System.out.println("### Timemeasure [TfEvent::deleteTdPrice]: "+(finish-start)+"ms");

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
