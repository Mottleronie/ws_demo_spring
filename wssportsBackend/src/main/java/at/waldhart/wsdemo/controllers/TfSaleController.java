package at.waldhart.wsdemo.controllers;

import at.waldhart.wsdemo.dtos.TdPriceDTO;
import at.waldhart.wsdemo.dtos.TfSaleDTO;
import at.waldhart.wsdemo.dtos.mappers.TdPriceDtoMapper;
import at.waldhart.wsdemo.dtos.mappers.TfSaleDtoMapper;
import at.waldhart.wsdemo.models.TdArticle;
import at.waldhart.wsdemo.models.TdPrice;
import at.waldhart.wsdemo.models.TfSale;
import at.waldhart.wsdemo.services.TdArticleService;
import at.waldhart.wsdemo.services.TdPriceService;
import at.waldhart.wsdemo.services.TfSaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static java.util.stream.Collectors.toList;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/sales")
public class TfSaleController {

    @Autowired
    TfSaleService TfSaleService;

    @Autowired
    TdArticleService TdArticleService;

    @Autowired
    TdPriceService TdPriceService;

    @Autowired
    TfSaleDtoMapper mapper;

    @Autowired
    TdPriceDtoMapper priceDtoMapper;

    @RequestMapping(value="/", method= RequestMethod.GET)
    @ResponseBody
    public List<TfSaleDTO> getAllTfSale(){
        long start = System.currentTimeMillis();
        List<TfSaleDTO> returnPayload = this.TfSaleService.getTfSaleDto();
        long finish = System.currentTimeMillis();
        System.out.println("### Timemeasure [TfSaleController::getAllTfSale]: "+(finish-start)+"ms");

        return returnPayload;
    }

    @RequestMapping(value="/{id}", method= RequestMethod.GET)
    @ResponseBody
    public TfSaleDTO getTfSale(@PathVariable final Long id){

        long start = System.currentTimeMillis();
        TfSaleDTO rtn = mapper.toDto(this.TfSaleService.getTfSale(id));
        long finish = System.currentTimeMillis();
        System.out.println("### Timemeasure [TfSaleController::getTfSale]: "+(finish-start)+"ms");

        return rtn;
    }

    @RequestMapping(value="/{id}/article", method= RequestMethod.GET)
    @ResponseBody
    public TdArticle getTdArticleOfSale(@PathVariable final Long id){

        long start = System.currentTimeMillis();
        TdArticle rtn = this.TdArticleService.getArticle(id);
        long finish = System.currentTimeMillis();
        System.out.println("### Timemeasure [TfSaleController::getTdArticleOfSale]: "+(finish-start)+"ms");

        return rtn;
    }

    @RequestMapping(value="/{id}/price", method= RequestMethod.GET)
    @ResponseBody
    public TdPriceDTO getTfPriceOfSale(@PathVariable final Long id){

        long start = System.currentTimeMillis();
        TdPriceDTO rtn = priceDtoMapper.toDto(this.TdPriceService.getTdPrice(id));
        long finish = System.currentTimeMillis();
        System.out.println("### Timemeasure [TfSaleController::getTdArticleOfSale]: "+(finish-start)+"ms");

        return rtn;
    }

    @RequestMapping(value="/", method= RequestMethod.POST)
    @ResponseBody
    public TfSale createTfSale(@RequestBody final TfSale newEntity){

        long start = System.currentTimeMillis();
        TfSale rtn = this.TfSaleService.createTfSale(newEntity);
        long finish = System.currentTimeMillis();
        System.out.println("### Timemeasure [TfSaleController::createTfSale]: "+(finish-start)+"ms");

        return rtn;
    }

    @RequestMapping(value="/{id}", method= RequestMethod.PUT)
    @ResponseBody
    public TfSale updateTfSale(@RequestBody final TfSale updateEntity, @PathVariable final Long id){
        long start = System.currentTimeMillis();
        TfSale rtn = this.TfSaleService.updateTfSale(id, updateEntity);
        long finish = System.currentTimeMillis();
        System.out.println("### Timemeasure [TfSaleController::updateTfSale]: "+(finish-start)+"ms");

        return rtn;
    }

    @RequestMapping(value="/{id}", method= RequestMethod.PATCH)
    @ResponseBody
    public TfSale patchTfSale(@RequestBody final String patch, @PathVariable final Long id){
        long start = System.currentTimeMillis();
        TfSale rtn = this.TfSaleService.patchTfSale(id, patch);
        long finish = System.currentTimeMillis();
        System.out.println("### Timemeasure [TfSaleController::patchTfSale]: "+(finish-start)+"ms");

        return rtn;
    }

    @RequestMapping(value="/{id}", method= RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<Object> deleteTfSale(@PathVariable final Long id){
        long start = System.currentTimeMillis();
        this.TfSaleService.deleteTfSale(id);
        long finish = System.currentTimeMillis();
        System.out.println("### Timemeasure [TfEvent::deleteTfSale]: "+(finish-start)+"ms");

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
