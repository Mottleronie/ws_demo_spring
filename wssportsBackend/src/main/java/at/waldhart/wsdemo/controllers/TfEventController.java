package at.waldhart.wsdemo.controllers;

import at.waldhart.wsdemo.dtos.TfEventDTO;
import at.waldhart.wsdemo.dtos.mappers.TfEventDtoMapper;
import at.waldhart.wsdemo.models.TdPrice;
import at.waldhart.wsdemo.models.TfEvent;
import at.waldhart.wsdemo.services.TfEventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static java.util.stream.Collectors.toList;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/events")
public class TfEventController {

    @Autowired
    TfEventService TfEventService;
    @Autowired
    TfEventDtoMapper mapper;

    @RequestMapping(value="/", method= RequestMethod.GET)
    @ResponseBody
    public List<TfEventDTO> getAllTfEvent(){
        long start = System.currentTimeMillis();
        List<TfEventDTO> returnPayload = this.TfEventService.getTfEvent().stream().map(mapper::toDto).collect(toList());;
        long finish = System.currentTimeMillis();
        System.out.println("### Timemeasure [TfEvent::getAllTfEvent]: "+(finish-start)+"ms");

        return returnPayload;
    }

    @RequestMapping(value="/{id}", method= RequestMethod.GET)
    @ResponseBody
    public TfEventDTO getTfEvent(@PathVariable final Long id){
        long start = System.currentTimeMillis();
        TfEventDTO rtn = mapper.toDto(this.TfEventService.getTfEvent(id));
        long finish = System.currentTimeMillis();
        System.out.println("### Timemeasure [TfEvent::getTfEvent]: "+(finish-start)+"ms");

        return rtn;
    }

    @RequestMapping(value="/", method= RequestMethod.POST)
    @ResponseBody
    public TfEvent createTfEvent(@RequestBody final TfEvent newEntity){
        long start = System.currentTimeMillis();
        TfEvent rtn = this.TfEventService.createTfEvent(newEntity);
        long finish = System.currentTimeMillis();
        System.out.println("### Timemeasure [TfEvent::createTfEvent]: "+(finish-start)+"ms");

        return rtn;
    }

    @RequestMapping(value="/{id}", method= RequestMethod.PUT)
    @ResponseBody
    public TfEvent updateTfEvent(@RequestBody final TfEvent updateEntity, @PathVariable final Long id){
        long start = System.currentTimeMillis();
        TfEvent rtn = this.TfEventService.updateTfEvent(id, updateEntity);
        long finish = System.currentTimeMillis();
        System.out.println("### Timemeasure [TfEvent::updateTfEvent]: "+(finish-start)+"ms");

        return rtn;
    }

    @RequestMapping(value="/{id}", method= RequestMethod.PATCH)
    @ResponseBody
    public TfEvent patchTfEvent(@RequestBody final String patch, @PathVariable final Long id){
        long start = System.currentTimeMillis();
        TfEvent rtn = this.TfEventService.patchTfEvent(id, patch);
        long finish = System.currentTimeMillis();
        System.out.println("### Timemeasure [TfEvent::patchTfEvent]: "+(finish-start)+"ms");

        return rtn;
    }

    @RequestMapping(value="/{id}", method= RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<Object> deleteTfEvent(@PathVariable final Long id){
        long start = System.currentTimeMillis();
        this.TfEventService.deleteTfEvent(id);
        long finish = System.currentTimeMillis();
        System.out.println("### Timemeasure [TfEvent::deleteTfEvent]: "+(finish-start)+"ms");

        return new ResponseEntity<>(HttpStatus.OK);
    }

}
