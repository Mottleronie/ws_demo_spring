drop table if exists tf_sale;

-- ca 10000 Verkäufe
CREATE TABLE tf_sale (
                         SAL_NO serial not null,
                         SAL_ART_NO BIGINT NOT NULL,
                         SAL_PRC_NO BIGINT NOT NULL,
                         SAL_TYPE smallint not null, -- 1 reservation, 2 sale
                         SAL_DATE_FROM DATE not null,
                         SAL_DATE_TO DATE not null,
                         SAL_ADDITONAL_PROPERTIES JSONB
);

ALTER TABLE "public".tf_sale ADD CONSTRAINT pky_tf_sale_00 PRIMARY KEY (sal_no);
ALTER TABLE "public".tf_sale ADD CONSTRAINT fky_salart_00 FOREIGN KEY (sal_art_no) REFERENCES "public".td_article(art_no);
ALTER TABLE "public".tf_sale ADD CONSTRAINT fky_salprc_01 FOREIGN KEY (sal_prc_no) REFERENCES "public".td_price(prc_no);