drop table if exists td_price;
-- für jeden artikel einen preis 01.01.xxx - 31.12.xxx -> ab 2019 - 2022
CREATE TABLE td_price (
                          PRC_NO serial not null,
                          PRC_ART_NO BIGINT NOT NULL,
                          PRC_DATE_FROM DATE not null,
                          PRC_DATE_TO DATE not null,
                          PRC_PRICE SMALLINT NOT NULL,
                          PRC_PERCENTAGE SMALLINT NOT NULL
);

ALTER TABLE "public".td_price ADD CONSTRAINT pky_td_price_00 PRIMARY KEY (prc_no);
ALTER TABLE "public".td_price ADD CONSTRAINT fky_prcart_00 FOREIGN KEY (prc_art_no) REFERENCES "public".td_article(art_no);