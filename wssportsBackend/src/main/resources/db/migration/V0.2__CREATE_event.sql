drop table if exists tf_event;

-- für jeden artikel einen starttag von 01.01.xxx - 31.12.xxx -> ab 2019 - 2022
-- nur zu testzwecken wie schnell insert geht und abfrage - würde article_scheduledevent entsprechen
-- ob das in Zukunft weiterhin so sein wird werden wir sehen
CREATE TABLE tf_event (
                          EVN_NO serial not null,
                          EVN_ART_NO BIGINT NOT NULL,
                          EVN_DATE_STARTDAY DATE not null
);

ALTER TABLE "public".tf_event ADD CONSTRAINT pky_tf_event_00 PRIMARY KEY (evn_no) ;
ALTER TABLE "public".tf_event ADD CONSTRAINT fky_evnart_00 FOREIGN KEY (evn_art_no) REFERENCES "public".td_article(art_no) ;