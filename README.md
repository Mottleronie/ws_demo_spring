# ws_demo_spring

This is a demo project to investigate the following Tech-Stack:
* BE: Java Spring Boot
* FE: Angular 2


## Run the project (development):

```
cd wssportsBackend
mvn spring-boot:run
```

Then the backend is available at `http://localhost:8080`

## Build project

```
cd wssportsBackend
mvn package
```

This creates a .jar file in the `wssportsBackend/target/` folder. It is executable
by running:

```
java -jar <filename>.jar
```

## Migrations

Before you can perform migrations you have to setup flyway correctly.
The database credentials are stored in the root of the backend submodule `wssportsBackend/flyway.conf`

Afterwards migrations can be run by:
```
mvn clean flyway:migrate -Dflyway.configFiles=flyway.conf
```

#### Writing new migrations:

In order to make database changes it is necessary to add
the corresponding changes in a migration file. Those files
are placed in `wssportsBackend/src/main/resources/db/migration/`

The file scheme is
```
V<version-number>__<description>.sql

e.g.: V1.3__CREATE_infotable
```

# Project structure

This project is mainly splitted into:
* `wssportsBackend`: The BE, implemented as Java Spring-Boot project (maven)
* `wssportsFrontend`:

## The Backend:

* `pom.xml`: The pom.xml file contains the major project information 
like version etc. and the maven dependencies. You can search for additional deps on this page [https://mvnrepository.com/](MVN-repository)
* `target`: This is the build folder with the executable `.jar` file.
* `src`: main folder with all the source code
  * `main`: productive code
    * `java/at/waldhart/wsdemo`: the java source code
      * `config`: here can be made Spring Boot configurations
      * `models`: all db entities
      * `dtos`: **D**ata **T**ransfer **O**bjects for the entities in `models`
        * `mappers`: corresponding mappers from DTOs to models
      * `errors`: all classes for error handling
        * `exceptions`: custom exceptions
      * `controllers`: api routes
      * `services`: business logic for operating on the entities
      * `repositories`: db access layer
      * `utility`: helper functions
    * `resources`: additional configuration files (e.g. `application.properties`) and db migration files
  * `test`: all testing files are collected here

#### add Model:

For adding a model we make use of `POJOs` ("**P**lain **O**ld **J**ava **O**bjects").

```java
@Entity
public class TdArticle {
    
  /* define a primary key */  
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(columnDefinition = "serial")
  private Long artNo;

  @Size(max=100)
  private String artName;

  /* if a specific datatype has to be defined */
  @Type(type = "org.hibernate.type.ShortType")
  private int artType;


  /* getter and setter */
  public Long getArtNo() { return artNo; }
  public String getArtName() { return artName; }  
  public void setArtNo(Long artNo) { this.artNo = artNo; }
  public void setArtName(String artName) { this.artName = artName; }


  @Override
  public String toString() {
    return "[Article#"+this.artNo+"]: {artName="+this.artName+", artType="+this.artType+", artActive="+this.artActive+"}";
  }

}
```

#### add DTO with its mapper:

`DTO` stands for "**D**ata **T**ransfer **O**bject". They are used to add another level of abstraction and
decouple the persistence from the presentation layer. This allows us to create kind of `views` on objects
for different routes and provides us more flexibility. DTOs are internally simple POJOs
that have in addition a mapper class for transferring the data from the database to the JSON response.


```java
// DTO
public class TdPriceDTO {

  private Long prcNo;
  private Long prcArticle; // --> notice that here we return just the ID instead of the whole TdArticle object
  private Date prcDateFrom;
  private Date prcDateTo;
  private Short prcPrice;
  private Short prcPercentage;


  public Long getPrcArticle() { return prcArticle; }

  public void setPrcArticle(final Long prcArticle) { this.prcArticle = prcArticle; }

  public Date getPrcDateFrom() { return prcDateFrom; }

  public void setPrcDateFrom(final Date prcDateFrom) { this.prcDateFrom = prcDateFrom; }

  public Date getPrcDateTo() { return prcDateTo; }

  public void setPrcDateTo(final Date prcDateTo) { this.prcDateTo = prcDateTo; }

  public Short getPrcPrice() { return prcPrice; }

  public void setPrcPrice(final Short prcPrice) { this.prcPrice = prcPrice; }

  public Short getPrcPercentage() { return prcPercentage; }

  public void setPrcPercentage(final Short prcPercentage) { this.prcPercentage = prcPercentage; }

  public Long getPrcNo() {
    return prcNo;
  }

  public void setPrcNo(final Long prcNo) { this.prcNo = prcNo; }

}
```

```java
// Mapper
@Component
public class TdPriceDtoMapper {
  public TdPriceDTO toDto(final TdPrice originalModel) {
    TdPriceDTO newDto = new TdPriceDTO();

    newDto.setPrcNo(originalModel.getPrcNo());
    newDto.setPrcPercentage(originalModel.getPrcPercentage());
    newDto.setPrcDateTo(originalModel.getPrcDateTo());
    newDto.setPrcDateFrom(originalModel.getPrcDateFrom());
    newDto.setPrcPrice(originalModel.getPrcPrice());
    // just assign TdArticle ID instead of whole object
    newDto.setPrcArticle(originalModel.getPrcArticle().getArtNo());

    return newDto;
  }
}
```

Later in the corresponding controller we make use of the DTO class by replacing it in the
function signatures and also add the the mappers calls of `toDto()`

```java
// in the controller
// replace TdPrice by TdPriceDTO
public List<TdPriceDTO> getAllTdPrice(){
  List<TdPriceDTO> returnPayload = this.TdPriceService.getTdPrice()
                                    .stream() // get return value as stream...
                                    .map(mapper::toDto) //...to pass it to the mapper and map them to DTO
                                    .collect(toList()); // pack all objects to a list again
  
  return returnPayload;
}

```

#### add Repository:

The repository of an entity represents its database layer.
So all methods that are required to store, get and update entities in the db are handled here.
It is defined as an `interface` which means we do not have to implement any functions
in most cases because the function signature follows a specific scheme 
and hibernate creates all the required functions for us:

```java
public interface TdArticleRepository extends CrudRepository<TdArticle, Long> {

  List<TdArticle> findByArtName(String artName); // SELECT * FROM td_article where art_name = <artName>; 
  List<TdArticle> findAll(); // SELECT * FROM td_article;
  TdArticle findByArtNo(long artNo); // SELECT * FROM td_article where art_no = <artNo> LIMIT 1;
}
```

#### add Controller:

The controller class is used to trigger user interaction on a models. So it mainly
defines the API-routes.

```java
@RestController
@RequestMapping("api/article") // main route
public class TdArticleController {

  @Autowired
  TdArticleService articleService;

  // GET-route for fetching all
  @RequestMapping(value="/", method= RequestMethod.GET)
  @ResponseBody
  public List<TdArticle> getAllTdArticles(){
    List<TdArticle> returnPayload = this.articleService.getArticles();
    return returnPayload;
  }

  // GET-route for fetching one article
  @RequestMapping(value="/{artNo}", method= RequestMethod.GET)
  @ResponseBody
  public TdArticle getTdArticle(@PathVariable Long artNo){
    return this.articleService.getArticle(artNo);
  }

  // POST-route for creating new entities 
  @RequestMapping(value="/", method= RequestMethod.POST)
  @ResponseBody
  public TdArticle createTdArticle(@RequestBody TdArticle newEntity){
    return this.articleService.createArticle(newEntity);
  }

  // PUT-route for updating existing objects
  @RequestMapping(value="/{artNo}", method= RequestMethod.PUT)
  @ResponseBody
  public TdArticle updateTdArticle(@RequestBody TdArticle updateEntity, @PathVariable Long artNo){
    return this.articleService.updateArticle(artNo, updateEntity);
  }

  // PATCH-route for partial updates on entities
  @RequestMapping(value="/{artNo}", method= RequestMethod.PATCH)
  @ResponseBody
  public TdArticle patchTdArticle(@RequestBody String patch, @PathVariable Long artNo){
    return this.articleService.patchArticle(artNo, patch);
  }

  // DELETE-route to remove 
  @RequestMapping(value="/{id}", method= RequestMethod.DELETE)
  @ResponseBody
  public ResponseEntity<Object> deleteTdArticle(@PathVariable Long id){
    long finish = System.currentTimeMillis();    
    return new ResponseEntity<>(HttpStatus.OK);
  }
}
```

## Scripts

In the folder `scripts/` you can find some helper bash-scripts for a more convenient development.

#### add_model.sh:

This script creates for a given <ModelName> the corresponding `model` and the required `controller`, `service` and `repository`

```bash
bash scripts/add_model <YourModelName>
```

## Error handling

source: [https://www.baeldung.com/exception-handling-for-rest-with-spring#controlleradvice]()

We make use of a global error handling component that is defined with the help of the
`@ControllerAdvice` annotation. The class `WsErrorHandler` is the main error handler.
If an exception is thrown and not caught and handled then `WsErrorHandler` catches
the exception and generates the corresponding response. With this method we do not have to catch
Exceptions in controllers since they are globally handled.

To be able to define different error messages and codes we define custom exceptions in
the package `at.waldhart.wsdemo.errors.exceptions`. Such a sample exception
looks like this:

```java
public class EntityNotFoundException extends RuntimeException {
    private static final String defaultMessage = "This is the default error message";

    // default constructor
    public EntityNotFoundException() { super(EntityNotFoundException.defaultMessage); }

    // exception with parameters
    public EntityNotFoundException(Class entityClass, Long requestedId) {
        super("The requested "+entityClass.getName()+" with ID="+requestedId+" could not be found...");
    }
}
```

To handle the exception in the correct way we define an `ErrorResponse` in 
`WsErrorHandler` like in the following example:

```java
@ControllerAdvice
public class WsErrorHandler extends ResponseEntityExceptionHandler {

  @ExceptionHandler(EntityNotFoundException.class) // exception handled
  public ResponseEntity<ErrorResponse> handleNullPointerExceptions(Exception e) {
    // ... potential custom logic
    HttpStatus status = HttpStatus.NOT_FOUND; // 404

    return new ResponseEntity<>(
            new ErrorResponse(status, e.getMessage()), status);
  }
}
```

## The Frontend:

This project's frontend is setup with Angular ([https://angular.io/]()) and located in
the folder `wssportsFrontend`.

### TODO: add CLI section

Angular provides a CLI that enhances the development process. The most important commands are:

```shell
ng serve                                    # runs the angular server on localhost:4200
ng generate module <modulename>             # creates a new module with all its necessary files
ng generate component <componentname>       # adds a new component into the given module and references it already correctly
ng generate service <servicetname>          # adds a new component into the given module and references it already correctly
```

### Project structure:

The frontend project is structured as follows:
* `*.json`: Those files are mainly for configuration
* `src`: main folder with all the source code
  * `assets`: assets sources like favicons, fonts,...
  * `environments`: here different environment variables can be set 
  * `app`: productive code
    * `articles,...`: Those are modules, so they summarize a set of components that are logically related
      * `article, event,...`: Those are the concrete components, so the building blocks of the app
        * `*.html`: The template file with the HTML-structure
        * `*.scss`: All component-specific styles
        * `*.specs.ts`: "Specifications" - This is the testing suite for the component
        * `*.ts`: The business logic of the component
      * `models.ts`: The backend models must be also modelled in the FE for the mapping from the DB entities to the presentation layer
      * `*.service.ts`: Services are handling the API-requests and return an observable
      * `*.module.ts`: The configuration of the current module

### Make a BE-request:

In order to interact with the BE we need a possibility to communicate with it.
For this we are firing HTTP(s)-requests. The BE provides us `routes` which are
called by the `Service` classes. 

The `Service` provides functions that are returning `Observables`. In the given example we have 
a `Service` for dealing with the `Sales` entities. In case we want to fetch all of them
we have to call in the FE the function `getSales()`. The `injector` is simply used for decoupling, because
in case the route changes its names we only have to fix the `"sales_prefix"` value in
the corresponding `module.ts`.

```typescript
export class SalesService {

  constructor(private injector: Injector, private http: HttpClient) { }

  public getSales(): Observable<WsSale[]> {
      // get request on the route /sales
    return this.http.get<WsSale[]>(environment.apiBaseUrl + this.injector.get('sales_prefix'));
  }
}
```

In the components we can access those `Services` by injecting them in the constructor
and afterwards subscribe to the returned `Observable`:

```typescript
export class SaleComponent extends PageComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private titleService: Title,
    private salesService: SalesService   // injecting Service
  ) {
    super(router, route, titleService);
  }

  ngOnInit(): void {
    this.salesService.getSales().pipe(first()).subscribe(
      resp => {
        // ... do stuff with returned response
      },
      err => {console.log('handle error!', err);}
    );
  }

}
```




## Tests

### Backend
All backend tests are located in the folder `wssportsBackend/src/test`.
There is the same package structure given as for the source code. For unit tests
it is possible to write one testsuite (= one test class) for each
corresponding java class in the source code.

A simple unit test looks like this:

```java
@SpringBootTest // make sure this annotation is provided
class YourClassToInvestigateTests {

	@Test // every function
	void testAddTwoNumbers() {
        int x = 5;
        int y = 3;
        int z = YourClassToInvestigate.addTwoNumbers(x, y);
        
        // asserts let the test fail if the results differs from the expected value
        assertEquals(8, z);
	}

}
```
