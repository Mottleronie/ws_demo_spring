#!/bin/bash
echo "Start creating model with MVC pattern"
MAINPATH="wssportsBackend/src/main/java/at/waldhart/wsdemo/"

read -r -d '' MODELTEMPLATE << EOM
package at.waldhart.wsdemo.models;

import javax.persistence.*;

@Entity
public class $1 {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Long id;

    /* getter and setter */
    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    // TODO: adapt with new fields
    @Override
    public String toString() {
        return "[#"+this.id+"]: {}";
    }

}
EOM

read -r -d '' SERVICETEMPLATE << EOM
package at.waldhart.wsdemo.services;

import at.waldhart.wsdemo.models.$1;
import at.waldhart.wsdemo.repositories.$1Repository;

import at.waldhart.wsdemo.errors.exceptions.CannotUpdateNonExistingEntity;
import at.waldhart.wsdemo.errors.exceptions.EntityNotFoundException;
import at.waldhart.wsdemo.errors.exceptions.UpdateEntityFailedException;

import at.waldhart.wsdemo.utility.WsUtility;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class $1Service {

    @Autowired
    $1Repository $1Repo;

    @Autowired
    ObjectMapper objectMapper;

    public List<$1> get$1() {
            return this.$1Repo.findAll();
        }

        public $1 get$1(Long id) {
            $1 foundEntity = this.$1Repo.findById(id);
            if(foundEntity == null){
                throw new EntityNotFoundException(id, $1.class);
            }
            return foundEntity;
        }

        public $1 create$1($1 newEntity) {
            return this.$1Repo.save(newEntity);
        }

        public $1 update$1(Long id, $1 updateEntity) {
            if(this.$1Repo.findById(id) == null){
                throw new CannotUpdateNonExistingEntity(id, $1.class);
            }

            updateEntity.setId(id);

            try{
                return this.$1Repo.save(updateEntity);
            }
            catch(Exception e){
                throw new UpdateEntityFailedException(id, updateEntity);
            }
        }

        public $1 patch$1(Long id, String patchRequestString){
            try {
                $1 originalEntity = this.get$1(id);
                $1 patchEntity = objectMapper.readValue(patchRequestString, $1.class);

                List<String> deletedFieldsList = new ArrayList<>();
                Map<String, Object> objToPatch = objectMapper.readValue(patchRequestString, Map.class);
                for (Map.Entry<String, Object> entry : objToPatch.entrySet()) {
                    if(entry.getValue() == null){
                        deletedFieldsList.add(entry.getKey());
                    }
                }

                originalEntity = WsUtility.patchObjects(patchEntity, originalEntity, deletedFieldsList);

                return this.update$1(id, originalEntity);
            }
            catch (Exception e) {
                System.out.println(e);
                throw new CannotUpdateNonExistingEntity(id);
            }
        }

}
EOM

read -r -d '' CONTROLLERTEMPLATE << EOM
package at.waldhart.wsdemo.controllers;

import at.waldhart.wsdemo.models.$1;
import at.waldhart.wsdemo.services.$1Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/ROUTE")
public class $1Controller {

    @Autowired
    $1Service $1Service;

    @RequestMapping(value="/", method= RequestMethod.GET)
    @ResponseBody
    public List<$1> getAll$1(){
        List<$1> returnPayload = this.$1Service.get$1();
        return returnPayload;
    }

    @RequestMapping(value="/{id}", method= RequestMethod.GET)
    @ResponseBody
    public $1 get$1(@PathVariable Long id){
        return this.$1Service.get$1(id);
    }

    @RequestMapping(value="/", method= RequestMethod.POST)
    @ResponseBody
    public $1 create$1(@RequestBody $1 newEntity){
        return this.$1Service.create$1(newEntity);
    }

    @RequestMapping(value="/{id}", method= RequestMethod.PUT)
    @ResponseBody
    public $1 update$1(@RequestBody $1 updateEntity, @PathVariable Long id){
        return this.$1Service.update$1(id, updateEntity);
    }

    @RequestMapping(value="/{id}", method= RequestMethod.PATCH)
    @ResponseBody
    public $1 patch$1(@RequestBody String patch, @PathVariable Long id){
        return this.$1Service.patch$1(id, patch);
    }
}

EOM

read -r -d '' REPOSITORYTEMPLATE << EOM
package at.waldhart.wsdemo.repositories;

import at.waldhart.wsdemo.models.$1;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface $1Repository extends CrudRepository<$1, Long> {

    List<$1> findAll();

    $1 findById(long id);
}
EOM


echo "$MODELTEMPLATE" > $MAINPATH"models/$1.java"
echo $MAINPATH"$1.java"

echo "$SERVICETEMPLATE" > $MAINPATH"services/$1Service.java"
echo $MAINPATH"$1Service.java"

echo "$CONTROLLERTEMPLATE" > $MAINPATH"controllers/$1Controller.java"
echo $MAINPATH"$1Controller.java"

echo "$REPOSITORYTEMPLATE" > $MAINPATH"repositories/$1Repository.java"
echo $MAINPATH"$1Repository.java"

echo "Finished creating model with MVC pattern"

