import { Component, OnInit } from '@angular/core';
import {PageComponent} from "../../wssports/structure/page.component";
import {ActivatedRoute, Router} from "@angular/router";
import {Title} from "@angular/platform-browser";
import {Article} from "../models";
import {ArticlesService} from "../articles.service";
import {first} from "rxjs/operators";

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent extends PageComponent implements OnInit {

  public title: string = 'Artikel';

  public allArticles: Article[] = [];
  public articlesLoading: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private titleService: Title,
    private articlesService: ArticlesService
  ) {
    super(router, route, titleService);
  }

  ngOnInit(): void {
    this.articlesLoading = true;
    this.articlesService.getArticles().pipe(first()).subscribe(
      resp => {
        console.log('DEBUGGGGGGG----getArticles', resp);
        if(resp && resp.length > 0){
          this.allArticles = resp;
        }
        else{
          this.allArticles = [];
        }

      },
      err => {
        console.log('DEBUGGGGGGG-----getArticles---ERROR!', err);
      }
    ).add(() => {
      this.articlesLoading = false;
    });
  }

}
