export class Article{
  artNo: number = -1;
  artName: string = '';
  artType: number = 0;
  artActive: number = 0;
  artInformation: any[] = [];
}

export class WsEvent{
  evnNo: number = -1;
  evnArticle: number = -1;
  evnDateStartday: Date = new Date();
}

export class WsPrice{
  prcNo: number = -1;
  prcArticle: number = -1;
  prcDateFrom: Date = new Date();
  prcDateTo: Date = new Date();
  prcPrice: number = 0.0;
  prcPercentage: number = 0.0;
}
