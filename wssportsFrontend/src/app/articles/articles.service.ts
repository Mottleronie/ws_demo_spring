import {Injectable, Injector} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Article, WsEvent, WsPrice} from "./models";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ArticlesService {

  constructor(
    private injector: Injector,
    private http: HttpClient) { }

  public getArticles(): Observable<Article[]> {
    return this.http.get<Article[]>(environment.apiBaseUrl + this.injector.get('articles_prefix'));
  }

  public getEvents(): Observable<WsEvent[]> {
    return this.http.get<WsEvent[]>(environment.apiBaseUrl + this.injector.get('events_prefix'));
  }

  public getPrices(): Observable<WsPrice[]> {
    return this.http.get<WsPrice[]>(environment.apiBaseUrl + this.injector.get('prices_prefix'));
  }
}
