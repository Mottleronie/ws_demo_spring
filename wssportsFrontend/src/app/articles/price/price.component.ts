import { Component, OnInit } from '@angular/core';
import {PageComponent} from "../../wssports/structure/page.component";
import {Article, WsPrice} from "../models";
import {ActivatedRoute, Router} from "@angular/router";
import {Title} from "@angular/platform-browser";
import {ArticlesService} from "../articles.service";
import {first} from "rxjs/operators";

@Component({
  selector: 'app-price',
  templateUrl: './price.component.html',
  styleUrls: ['./price.component.scss']
})
export class PriceComponent extends PageComponent implements OnInit {

  public title: string = 'Preise';
  public pricesLoading: boolean = false;
  public allPrices: WsPrice[] = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private titleService: Title,
    private articlesService: ArticlesService
  ) {
    super(router, route, titleService);
  }

  ngOnInit(): void {
    this.pricesLoading = true;
    this.articlesService.getPrices().pipe(first()).subscribe(
      resp => {
        console.log('DEBUGGGGGGG----getPrices', resp);
        if(resp && resp.length > 0){
          this.allPrices = resp;
        }
        else{
          this.allPrices = [];
        }

      },
      err => {
        console.log('DEBUGGGGGGG-----getArticles---ERROR!', err);
      }
    ).add(() => {this.pricesLoading = false;});
  }
}
