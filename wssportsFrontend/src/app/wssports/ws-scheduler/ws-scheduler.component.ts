import {Component, OnInit, Input, EventEmitter, Output, ViewChild, ElementRef, OnDestroy} from '@angular/core';
import { differenceInHours, set, differenceInMinutes } from 'date-fns';
import {moveItemInArray} from "@angular/cdk/drag-drop";
import {WsUtil} from "../helpers/wsutil.component";
import {FormControl} from "@angular/forms";
import {Subscription} from "rxjs";

export enum CalendarViewType {
  day = 0,
  month
};

@Component({
  selector: 'app-ws-scheduler',
  templateUrl: './ws-scheduler.component.html',
  styleUrls: ['./ws-scheduler.component.scss']
})
export class WsSchedulerComponent implements OnInit, OnDestroy {

  public CalendarViewType = CalendarViewType;
  public currentCalendarView: CalendarViewType = CalendarViewType.day;

  @ViewChild('wsSchedulerResourcesTable') wsSchedulerResourcesTable!: ElementRef;

  @Input() resourceLabel: string = 'Skilehrer';
  @Input() dayStart: any;
  @Input() dayEnd: any;
  @Input() resources: any;
  @Input() unassignedEvents: any;
  @Input() theme: 'material' | 'gradient' | null = 'material';
  @Output() saveEventChanges = new EventEmitter<any>();
  @Output() triggerEventEdit = new EventEmitter<any>();
  public dayStartHour: number = 0;
  public today = new Date();
  public selectedDate = this.today;
  public workingHours: number = 8;
  public weekDays: Date[] = [];
  public resourceCounter = 0;
  public maxEndTime: any;

  public currentDate: Date = new Date();

  public currentDateFormControl: FormControl = new FormControl('');
  private currentDateSubscription: Subscription | undefined = undefined;
  constructor() {}

  ngOnDestroy(): void {
    if(this.currentDateSubscription != undefined){
      this.currentDateSubscription.unsubscribe();
    }

  }

  ngOnInit(): void {
    this.getMaxEndTime();
    this.prepareChart();
    this.prepareResources();

    if(this.currentDateSubscription != undefined){this.currentDateSubscription.unsubscribe();}

    this.currentDateSubscription = this.currentDateFormControl.valueChanges.subscribe(
      next => {
        console.log('DEBUGGGGG----curretDateChanges: ', next);
      }
    );
    console.log(this.currentDate);
    console.log(this.resources);
    //this.currentDateFormControl = [];
    //for(let c=0; c<7; c++){
    //  this.weekDays.push(this.currentDate + WsUtil.)
    //}
  }

  updateDate(ev: any): void {
    console.log('DEBUGGGGGGG----updateDate', ev);
  }

  getMaxEndTime(): void {
    const [oldHours, oldMinutes] = this.dayEnd.split(':');
    let newHours = +oldHours + 1 + ((+oldMinutes > 0) ? 1 : 0);
    this.maxEndTime = ((newHours<10) ? '0'+newHours : newHours)+':00';
  }

  prepareChart() {
    this.dayStartHour = this.getHourFromTime(this.dayStart);
    this.workingHours = this.diffFromTime(this.dayEnd, this.dayStart, 'hours') + 2;
  }

  prepareResources() {
    this.resourceCounter = 0;
    this.resources.map((resource: any) => {
      this.resourceCounter += 1;
      if (resource.eventsList) {
        resource.eventsList.map((currentEvent: any, index: any) => {
          currentEvent.width = this.diffFromTime(currentEvent.end, currentEvent.start, 'minutes') * 2;
          currentEvent.offset = this.diffFromTime(currentEvent.start, this.dayStart, 'minutes') * 2;

        });
      }
    });
  }

  onTaskClick(clickedResource: any) {
    if (clickedResource.isParent) {
      this.resources.filter((currentEvent: any) => {
        if (currentEvent.parentID === clickedResource.id) {
          currentEvent.isHidden = !currentEvent.isHidden;
          clickedResource.active = !clickedResource.active;
        }
      });
    }
  }
  getHourFromTime(timeStr: any) {
    return Number(timeStr.split(':')[0]);
  }
  getMinuteFromTime(timeStr: any) {
    return Number(timeStr.split(':')[1]);
  }

  diffFromTime(endTime: any, StartTime: any, returnFormat: 'hours' | 'minutes') {

    const [endTimeHour, endTimeMinute] = endTime.split(':');
    const [StartTimeHour, StartTimeMinute] = StartTime.split(':');
    const taskEndDate = set(this.today, {
      hours: endTimeHour,
      minutes: endTimeMinute,
      seconds: 0
    });
    const taskStartDate = set(this.today, {
      hours: StartTimeHour,
      minutes: StartTimeMinute,
      seconds: 0
    });

    let res = 0;
    switch (returnFormat) {
      case 'hours':
        res = differenceInHours(new Date(taskEndDate), new Date(taskStartDate));
        break;
      case 'minutes':
        res = differenceInMinutes(
          new Date(taskEndDate),
          new Date(taskStartDate)
        );
        break;

      default:
        break;
    }
    return res;
  }

  boundaryElement = false ? '.ws-scheduler-events-resource-row' : '.ws-scheduler-events-body-container';

  dragDropped(ev: any, movedResource: any, movedEvent: any): void {

    let allResourcesElements = this.wsSchedulerResourcesTable.nativeElement.children;
    let newIdx = allResourcesElements.length + 1;
    for(let pidx=0; pidx<allResourcesElements.length; pidx += 1){
      if((ev.dropPoint.y < (allResourcesElements[pidx].getBoundingClientRect().y + 72)) &&
        (ev.dropPoint.y > (allResourcesElements[pidx].getBoundingClientRect().y))){
        newIdx = pidx;
      }
    }

    console.log('DEBUGGGGG---movedResource', movedResource, movedEvent);

    if((movedEvent.width > 0) || (movedEvent.offset > 0)){
      movedEvent.start = this.addMinutesToStringTime(movedEvent.start, (ev.distance.x/2));
      movedEvent.end = this.addMinutesToStringTime(movedEvent.end, (ev.distance.x/2));
    }

    if(movedEvent.start < this.dayStart){
      movedEvent.start = this.dayStart;
      movedEvent.end = this.addMinutesToStringTime(movedEvent.start, (movedEvent.width/2));
    }
    else if(movedEvent.end > this.maxEndTime){
      movedEvent.end = this.maxEndTime;
      movedEvent.start = this.addMinutesToStringTime(this.maxEndTime, (-movedEvent.width/2));
    }


    movedEvent.width = this.diffFromTime(movedEvent.end, movedEvent.start, 'minutes') * 2;

    movedEvent.offset = this.diffFromTime(movedEvent.start, this.dayStart, 'minutes') * 2;
    ev.source._dragRef.setFreeDragPosition({x: 0, y: 0});

    newIdx = Math.max(0, newIdx);
    // check if it is moved to unassigned
    if(newIdx >= this.resources.length){
      movedEvent.width = 0;
      movedEvent.offset = 0;
      ev.source._dragRef.reset();
      this.moveEvent((movedResource.eventsList) ? movedResource.eventsList : movedResource, this.unassignedEvents, movedEvent);
    }
    else{
      this.moveEvent((movedResource.eventsList) ? movedResource.eventsList : movedResource, this.resources[newIdx].eventsList, movedEvent);
    }
    console.log('DEBUGGGGG---movedResource', movedResource, movedEvent);
    this.saveEventChanges.emit(movedEvent);
  }

  addMinutesToStringTime(oldTime: string, addedMinutes: number): string {
    addedMinutes = Math.round(addedMinutes);
    const [oldHours, oldMinutes] = oldTime.split(':');
    addedMinutes += +oldMinutes + (+oldHours * 60);
    let minToAdd = addedMinutes%60;
    let hrToAdd = (addedMinutes-minToAdd)/60;
    return ((hrToAdd<10) ? '0'+hrToAdd : hrToAdd)+':'+((minToAdd<10) ? '0'+minToAdd : minToAdd);
  }

  resizeEndTime(ev: any, resource: any, event: any): void {
    event.width = Math.max(event.width + ev.event.movementX, 0);
    ev.source._dragRef.setFreeDragPosition({x: 0, y: 0});
    event.end = this.addMinutesToStringTime(event.start, Math.round(event.width/2));

  }

  resizeStartTime(ev: any, resource: any, event: any): void {
    event.width = Math.max(event.width - ev.event.movementX, 0);
    event.offset += ev.event.movementX;
    event.start = this.addMinutesToStringTime(event.end, Math.round(-event.width/2));
    ev.source._dragRef.setFreeDragPosition({x: 0, y: 0});
  }

  moveEvent(srcResourceList: any, targetResourceList: any, eventToMove: any): void {
    WsUtil.removeItemFromList(srcResourceList, eventToMove);
    targetResourceList.push(eventToMove);
  }

  triggerEdit(ev: any): void {
    this.triggerEventEdit.emit(ev);
  }

}
