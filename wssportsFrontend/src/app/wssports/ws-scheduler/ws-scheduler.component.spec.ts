import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WsSchedulerComponent } from './ws-scheduler.component';

describe('WsSchedulerComponent', () => {
  let component: WsSchedulerComponent;
  let fixture: ComponentFixture<WsSchedulerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WsSchedulerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WsSchedulerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
