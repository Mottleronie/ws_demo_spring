import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavigationComponent } from './navigation/navigation.component';
import {RouterModule} from "@angular/router";
import {PageComponent} from "./structure/page.component";
import { WsSchedulerComponent } from './ws-scheduler/ws-scheduler.component';
import {MatTooltipModule} from "@angular/material/tooltip";
import {DragDropModule} from "@angular/cdk/drag-drop";
import { WsSpinnerComponent } from './ws-spinner/ws-spinner.component';
import {CalendarModule} from "primeng/calendar";
import {PrimeDesignModule} from "./modules/primeng-design.module";
import {ReactiveFormsModule} from "@angular/forms";



@NgModule({
    declarations: [
        NavigationComponent,
        WsSchedulerComponent,
        WsSpinnerComponent
    ],
  exports: [
    NavigationComponent,
    WsSchedulerComponent,
    WsSpinnerComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    MatTooltipModule,
    DragDropModule,
    PrimeDesignModule,
    ReactiveFormsModule
  ]
})
export class WssportsModule { }
