export class WsSale{
  salNo: number = -1;
  salArticle: number = -1;
  salPrice: number = 0.0;
  salDateFrom: Date = new Date();
  salDateTo: Date = new Date();
  salType: number = 0;
  salAdditionalProperties: any[] = [];
}
