import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ArticleComponent} from "./articles/article/article.component";
import {HomeComponent} from "./welcome/home/home.component";
import {EventComponent} from "./articles/event/event.component";
import {PriceComponent} from "./articles/price/price.component";
import {SaleComponent} from "./sales/sale/sale.component";

const routes: Routes = [
  {path: 'articles', component: ArticleComponent},
  {path: 'events', component: EventComponent},
  {path: 'prices', component: PriceComponent},
  {path: 'sales', component: SaleComponent},
  {path: 'home', component: HomeComponent},
  {path: '', redirectTo: '/home', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
