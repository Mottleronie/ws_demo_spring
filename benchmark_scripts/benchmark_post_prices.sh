#!/bin/bash
echo "#########################################"
echo "### BENCHMARK POST REQUESTS (PRICES)  ###"
echo "#########################################"

make_request(){
	if [ -z "$4" ]
	then
		start=`date +%s.%N`
		curl --location --request $2 $3 -s -o benchmark_output.txt
		end=`date +%s.%N`
	else
		start=`date +%s.%N`
		curl --location --request $2 $3 -s -o benchmark_output.txt --header 'Content-Type: application/json' --data-raw "$4"
		end=`date +%s.%N`
	fi	

	runtime=$( echo "$end - $start" | bc -l )
	echo $3 ';' $2 ';' $runtime >> $1
}
export -f make_request

# reset output file
RESULTFILE=results/results_benchmark_post_prices.csv
rm $RESULTFILE
touch $RESULTFILE

# benchmark GET requests
echo 'route;method;runtime' >> $RESULTFILE

art=$(seq 1 3)
dates=("2019-01-01" "2019-03-17" "2019-04-01" "2019-07-02" "2019-09-25" "2019-11-11" "2020-01-01" "2020-03-17" "2020-04-01" "2020-07-02" "2020-09-25" "2020-11-11" "2021-01-01" "2021-03-17" "2021-04-01" "2021-07-02" "2021-09-25" "2021-11-11" "2022-01-01" "2022-03-17" "2022-04-01" "2022-07-02" "2022-09-25" "2022-11-11")

idx=$(seq 0 22)
arts=$(seq 0 10)

start_all=`date +%s.%N`
for artno in ${arts[@]}
do
	for i in $idx
	do
		make_request $RESULTFILE POST $1'/api/prices/' '{
			"prcDateFrom": "'${dates[$i]}'",
			"prcDateTo": "'${dates[$i+1]}'",
			"prcPrice": '$i',
			"prcPercentage": '$i',
			"prcArticle": {"artNo": '$artno'}
		}'
	done
done

end_all=`date +%s.%N`
runtime=$( echo "$end_all - $start_all" | bc -l )
echo 'ALL;POST;' $runtime >> $RESULTFILE
echo 'ALL;POST;' $runtime

start_all=`date +%s.%N`
for i in $idx
do
	seq 1 30 | xargs --replace={} -n1 -P8 bash -c 'make_request "$@"' _ $RESULTFILE POST $1'/api/prices/' '{
		"prcDateFrom": "'${dates[$i]}'",
		"prcDateTo": "'${dates[$i+1]}'",
		"prcPrice": '$i',
		"prcPercentage": '$i',
		"prcArticle": {"artNo": '{}'}
	}'
done
end_all=`date +%s.%N`
runtime=$( echo "$end_all - $start_all" | bc -l )
echo 'PARALLEL;POST;' $runtime >> $RESULTFILE
echo 'PARALLEL;POST;' $runtime

